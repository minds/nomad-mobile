var rnBridge = require('rn-bridge');

var message = require('./message');

const { join } = require('path');


const datService = require('./dat.service');


// Echo every message received from react-native
rnBridge.channel.on('message', async msg => {
  const { type, data, method, path, args  } = JSON.parse(msg);

  switch (method) {
    case 'init':
      datService.init(path);
      break;
    case 'close':
      break;
    case 'getFile':
      break;
  }

});

// Inform react-native node is initialized
//rnBridge.channel.send(message.ok());
