export class Profile {
  constructor(username, address, datUri) {
    this.username = username;
    this.address = address;
    this.datUri = datUri;
  }
}