import React, { Component } from 'react';
import { View, Text, TextInput, Button, StyleSheet } from 'react-native';
import paymentModalService from '../../services/payment-modal.service';

export default class Modal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            message: '',
            to: '',
            txValue: '',
            gasPrice: '',
            gasLimit: '',
        };
    }

    componentDidMount() {
        paymentModalService.setHandler(({ visible, message, to, txValue, gasPrice, gasLimit }) => {
            this.setState({
                visible,
                message,
                to,
                txValue,
                gasPrice,
                gasLimit
            });
        });
    }

    componentWillUnmount() {
        paymentModalService.clearHandler();
    }

    send() {
        this.setState({
            visible: false,
        });

        paymentModalService.callback({
            gasLimit: this.state.gasLimit,
            gasPrice: this.state.gasPrice,
        });
    }

    cancel() {
        this.setState({
            visible: false,
        });

        paymentModalService.callback(false);
    }

    render() {
        const { visible, message, to, txValue, gasPrice, gasLimit } = this.state;

        if (!visible) {
            return <View></View>;
        }

        return (
            <View style={[styles.overlay]}>
                <View style={[styles.modal]}>
                    <View>
                        <Text>{message}</Text>
                    </View>

                    <View>
                        <Text>{to}</Text>
                    </View>

                    {Boolean(txValue) && <View>
                        <Text>Amount: {txValue}</Text>
                    </View>}

                    <View>
                        <Text>Gas Price:</Text>
                        <TextInput
                            value={`${gasPrice}`}
                            onChangeText={gasPrice => this.setState({ gasPrice })}
                            placeholder="Gas price (in gwei)"
                        />
                    </View>

                    <View>
                        <Text>Gas Limit:</Text>
                        <TextInput
                            value={`${gasLimit}`}
                            onChangeText={gasLimit => this.setState({ gasLimit })}
                            placeholder="Gas price (in gwei)"
                        />
                    </View>

                    <View style={[styles.buttonBar]}>
                        <Button
                            style={[styles.button]}
                            title="Send"
                            onPress={this.send.bind(this)}
                        />

                        <Button
                            style={[styles.button]}
                            title="Cancel"
                            onPress={this.cancel.bind(this)}
                            color="#999"
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'transparent',
    },
    modal: {
        display: 'flex',
        position: 'absolute',
        bottom: 25,
        left: 25,
        right: 25,
        height: 500,
        padding: 25,
        backgroundColor: '#eeeeee',
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    buttonBar: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop: 25,
    },
});