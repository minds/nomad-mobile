import React, { Component } from 'react';
import { ActivityIndicator, AppRegistry, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';


export default class QRCodeReader extends Component {

    constructor(props) {
        super(props);

        this.state = {
            processing: false
        };
    }

    render() {
        console.log(this.state);
        if (this.state.processing) {
            return this.renderUploading();
        } else {
            return this.renderCamera();
        }
    }

    async qrDetected(event) {
        if (!event.barcodes || !event.barcodes[0]) {
            return;
        }

        const barcode = event.barcodes[0];

        if (barcode.type !== 'QR_CODE') {
            return;
        }

        this.setState({
            processing: true
        });

        if (this.$qrProcessingTimer) {
            clearTimeout(this.$qrProcessingTimer);
        }

        this.$qrProcessingTimer = setTimeout(() => {
            console.log('nomore');
            this.setState({
                processing: false,
            })
        }, 2500);

        try {
            await this.props.onBarcode(barcode.data);
        } catch (e) {
            console.warn(e);
        }
    }

    componentWillUnmount() {
        if (this.$qrProcessingTimer) {
            clearTimeout(this.$qrProcessingTimer);
        }
    }

    renderCamera() {
        return (<View style={styles.container}>
            <RNCamera
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
                permissionDialogTitle={'Permission to use camera'}
                permissionDialogMessage={'We can read QR Codes using your phone\'s camera.'}
                onGoogleVisionBarcodesDetected={this.qrDetected.bind(this)}
            />
        </View>);
    }

    renderUploading() {
        return <ActivityIndicator />;
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
    preview: {
        width: '100%',
        height: '100%',
    }
});
