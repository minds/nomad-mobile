class PaymentModalService {
  constructor() {
    this.handler = null;
    this.baseSendOptions = {};
    this.currentModalCallback = null;
  }

  setHandler(fn) {
    this.handler = fn;
  }

  clearHandler() {
    this.handler = null;
  }

  callback(result) {
    if (!this.currentModalCallback) {
      return;
    }

    this.currentModalCallback(result);
  }

  show(props = {}, baseSendOptions = {}) {
    this.handler({
      visible: true,
      ...props,
    });

    this.baseSendOptions = baseSendOptions;

    return new Promise(resolve => {
      this.currentModalCallback = result => {
        if (result && typeof result === 'object') {
          return resolve({ ...this.baseSendOptions, ...result });
        }
        
        return resolve(result);
      };
    });
  }

  hide() {
    this.handler({
      visible: false
    });

    this.currentModalCallback = null;
  }
}

export default new PaymentModalService;