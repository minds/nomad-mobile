const Web3 = require('web3');
const sign = require('ethjs-signer').sign;

import { BLOCKCHAIN_URI } from '../../config';
import paymentModalService from './payment-modal.service';

class Web3Service {
  // contractInstances = {};

  constructor() {
    this.web3 = new Web3(
      new Web3.providers.HttpProvider(BLOCKCHAIN_URI)
    );
  }

  // Wallets

  createWallet() {
    return this.web3.eth.accounts.create();
  }

  getAddressFromPK(privateKey) {
    if (!privateKey) {
      return null;
    }

    if (privateKey.substr(0, 2).toLowerCase() !== '0x') {
      privateKey = `0x${privateKey}`;
    }

    return this.web3.eth.accounts.privateKeyToAccount(privateKey).address;
  }

  // Eth

  async getBalance(address, unit = 'ether') {
    const balance = await this.web3.eth.getBalance(address);
    return this.web3.utils.fromWei(balance, unit);
  }

  // Contract methods

  async sendSignedContractMethod(from, method, message = '') {
    return await this.sendSignedContractMethodWithValue(from, method, 0, message);
  }

  async sendSignedContractMethodWithValue(from, method, value = 0, message = '') {
    let { privateKey, address } = from;

    if (privateKey.substr(0, 2).toLowerCase() !== '0x') {
      privateKey = `0x${privateKey}`;
    }

    const toHex = this.web3.utils.toHex,
      baseOptions = { from: address };

    let estimatedGas = 0;
    try {
      let latestBlock = await this.web3.eth.getBlock('latest');

      estimatedGas = await method.estimateGas({
        from: baseOptions.from,
        to: method._parent.options.address,
        gas: latestBlock.gasLimit,
        value: value,
      });
    } catch (e) {
      console.log(e);
    }

    // TODO: Show modal

    const sendOptions = await paymentModalService.show({
      message,
      to: method._parent.options.address,
      value,
      gasPrice: 1,
      gasLimit: Math.ceil(estimatedGas * 1.5),
    }, baseOptions);

    if (sendOptions) {
      const nonce = await this.web3.eth.getTransactionCount(sendOptions.from);

      const tx = {
        nonce,
        from: sendOptions.from,
        to: method._parent.options.address,
        data: method.encodeABI(),
        value: toHex(value),
        gas: toHex(parseInt(sendOptions.gasLimit, 10)),
        gasPrice: toHex(this.web3.utils.toWei(`${sendOptions.gasPrice}`, 'gwei')),
      }, signedTx = sign(tx, privateKey);

      return await new Promise((resolve, reject) => {
        this.web3.eth.sendSignedTransaction(signedTx)
          .once('transactionHash', hash => resolve({ transactionHash: hash }))
          .once('error', e => reject(e));
      });
    } else {
      throw new Error('E_CANCELLED');
    }
  }
}

export default new Web3Service();
