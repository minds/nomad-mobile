import React, { Component } from 'react';
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Image,
} from 'react-native';
import QRCodeReader from '../components/QRCodeReader';


import authService from '../services/auth.service';
import web3Service from '../services/web3.service';

export default class LoginKeyScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      inProgress: false,
      privateKey: '',
    };
  }

  async setPrivateKey(privateKey) {
    this.setState({ privateKey });

    const username = this.props.navigation.getParam('username');

    try {
      const address = web3Service.getAddressFromPK(privateKey);

      if (address) {
        this.setState({
          inProgress: true,
        });

        const profile = await authService.login(username, privateKey);

        if (profile) {
          this.props.navigation.replace('Home');
        } else {
          this.setState({
            inProgress: false,
          });
        }
      }
    } catch (e) {
      console.warn(e);
      this.setState({
        inProgress: false,
      });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image 
          source={require('../../assets/nomad.png')}
          style={{width: 50, height: 70}}
          />
        <TextInput
          style={[styles.pkInput]}
          value={this.state.privateKey}
          onChangeText={this.setPrivateKey.bind(this)}
          placeholder="Private Key"

        />

        <View style={styles.qrCodeReader}>
          {!this.state.inProgress && <QRCodeReader onBarcode={this.setPrivateKey.bind(this)} />}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  pkInput: {
    height: 50,
    borderRadius: 24,
    borderWidth: 1,
    borderColor: '#e8e8e8',
  },
  qrCodeReader: {
    width: '100%',
  }
});